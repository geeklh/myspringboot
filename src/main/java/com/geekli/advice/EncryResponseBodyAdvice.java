package com.geekli.advice;

import com.alibaba.fastjson.JSON;
import com.geekli.model.utils.Email;
import com.geekli.service.MailService;
import com.geekli.utils.DesUtil;
import com.geekli.utils.IpUtil;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@Component
@ControllerAdvice(basePackages = "com.geekli.controller")
@Slf4j
public class EncryResponseBodyAdvice implements ResponseBodyAdvice<Object> {
    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @Autowired
    private MailService mailService;

    @Override
    public Object beforeBodyWrite(Object obj, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse) {
        //通过 ServerHttpRequest的实现类ServletServerHttpRequest 获得HttpServletRequest
        ServletServerHttpRequest sshr = (ServletServerHttpRequest) serverHttpRequest;
        //此处获取到request 是为了取到在拦截器里面设置的一个对象 是我项目需要,可以忽略
        HttpServletRequest request = sshr.getServletRequest();

        String returnStr = "";

        try {
            //添加encry header，告诉前端数据已加密
            serverHttpResponse.getHeaders().add("encry", "true");
            String srcData = JSON.toJSONString(obj);
            //加密
            returnStr = DesUtil.encrypt(srcData);
//            returnStr = Desu.encrypt(srcData);
            String ipAddress = IpUtil.getIpAddr(request);//获取IP
            Email email = new Email();
            List<String> addressList = new ArrayList<String>();
            addressList.add("13414900681@163.com");
            email.setToAddress(addressList);
            email.setSubject("后端监控");
            email.setContent("Master! 有用户对后端进行了访问 <br><br> " +
                    "IP地址：" + ipAddress + "<br><br>" +
                    "访问成功了接口" + request.getRequestURI() + "<br><br>" +
                    "返回数据有：" + srcData + "<br><br>" +
                    "已经做了加密处理");
//            mailService.sendMail(email);//发送邮件
            log.info("发送请求的IP={}，接口={},返回原始数据={},加密后数据={}", ipAddress, request.getRequestURI(), srcData, returnStr);
//            log.info("接口={},返回原始数据={},加密后数据={}", request.getRequestURI(), srcData, returnStr);
        } catch (Exception e) {
            log.error("异常！", e);
        }
        return returnStr;
    }
}
