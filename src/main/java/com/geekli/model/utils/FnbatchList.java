package com.geekli.model.utils;

import java.util.List;

public class FnbatchList {
    private List<FrontuserNameList> appuserlist;

    public List<FrontuserNameList> getAppuserlist() {
        return appuserlist;
    }

    public void setAppuserlist(List<FrontuserNameList> appuserlist) {
        this.appuserlist = appuserlist;
    }
}
