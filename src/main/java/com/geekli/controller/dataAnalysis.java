package com.geekli.controller;

import io.swagger.annotations.Api;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin(maxAge = 3600)
@Api(value = "数据分析",tags = "数据分析")
@RequestMapping(value = "/dataAnalysis")
public class dataAnalysis {

}
