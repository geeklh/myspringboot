package com.geekli.controller;

import com.geekli.annotation.UserLoginToken;
import com.geekli.model.Userbackform;
import com.geekli.model.UserscKey;
import com.geekli.model.utils.MyResult;
import com.geekli.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin(maxAge = 3600)
@Api(tags = "管理员接口")
@RequestMapping(value = "/sadmin")
public class superadminController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/backuserlogin.do", method = RequestMethod.POST)
    @ApiOperation(value = "后台用户登录")
    @ResponseBody
    public MyResult backuserlogin(@RequestBody Userbackform userbackform) {//产生token密钥，测试
        return userService.backuserlogin(userbackform);
    }

    //验证测试
    @ResponseBody
    @UserLoginToken
    @GetMapping(value = "/getmessage.do")
    public String getMessage() {
        return "你已经通过验证";
    }

    @RequestMapping(value = "/backuserregist.do", method = RequestMethod.POST)
    @ApiOperation(value = "后台注册")
    @ResponseBody
    public MyResult backuserregist(@RequestBody Userbackform userbackform) {
        return userService.backuserregist(userbackform);
    }

    @RequestMapping(value = "/backdelete.do", method = RequestMethod.DELETE)
    @ApiOperation(value = "后台删除用户")
    @ResponseBody
    public MyResult backdelete(@RequestBody Userbackform userbackform) {
        return userService.backuserdelete(userbackform);
    }

    @RequestMapping(value = "/backupdateuser.do", method = RequestMethod.PUT)
    @ApiOperation(value = "后台更新用户")
    @ResponseBody
    public MyResult backupdateuser(@RequestBody Userbackform userbackform) {
        return userService.backupdateuser(userbackform);
    }

    @RequestMapping(value = "/userorder.do", method = RequestMethod.POST)
    @ApiOperation(value = "后台添加用户购买商品")
    @ResponseBody
    public MyResult userorder(@RequestBody UserscKey uk) {
        return userService.userorder(uk);
    }
}
