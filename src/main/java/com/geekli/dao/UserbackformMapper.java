package com.geekli.dao;

import com.geekli.model.Userbackform;

public interface UserbackformMapper {
    int deleteByPrimaryKey(String id);

    int insert(Userbackform record);

    int insertSelective(Userbackform record);

    Userbackform selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Userbackform record);

    int updateByPrimaryKey(Userbackform record);
}