package com.geekli.service.lmpl;

import com.auth0.jwt.JWT;
import com.geekli.dao.UserBehavior;
import com.geekli.model.Userbackform;
import com.geekli.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.auth0.jwt.algorithms.Algorithm;

import java.util.*;

@Service(value = "tokenService")
public class TokenServicelmpl implements TokenService {
    @Autowired
    private UserBehavior userBehavior;

    @Override
    public String getToken(Userbackform uf) {
        Date iatDate = new Date();//签发时间
        Calendar nowtime = Calendar.getInstance();
        nowtime.add(Calendar.HOUR, 168);//设置过期时间
        Date expiresDate = nowtime.getTime();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");
        String token = "";
        token = JWT.create()
                .withHeader(map)
                .withAudience(uf.getId())//将USERid加入token里面
                .withExpiresAt(expiresDate)//设置有效时间
                .withIssuedAt(iatDate)
                .sign(Algorithm.HMAC256(uf.getPassword()));
//        token = JWT.create().withAudience(uf.getId())// 将 user id 保存到 token 里面
//                .sign(Algorithm.HMAC256(uf.getPassword()));// 以 password 作为 token 的密钥
        return token;
    }
}
