package com.geekli.service;

import com.geekli.model.utils.Email;

public interface MailService {
    public void sendMail(Email email);
}
