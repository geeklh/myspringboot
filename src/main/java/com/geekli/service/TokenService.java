package com.geekli.service;

import com.geekli.model.Userbackform;

public interface TokenService {
    //后台用户获取token
    public String getToken(Userbackform userbackform);
}
