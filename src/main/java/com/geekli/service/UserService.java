package com.geekli.service;

import com.geekli.model.*;
import com.geekli.model.utils.FnbatchList;
import com.geekli.model.utils.FrontuserNameList;
import com.geekli.model.utils.MyResult;

import java.util.List;

public interface UserService {
    MyResult login(User user);

    MyResult regist(User user);

    MyResult deleteUser(FrontuserNameList fn);

    MyResult deleteUserProvider(FnbatchList fnbatchList);
//    MyResult deleteUserProvider(List<FrontuserNameList> fnlist);

    MyResult userorder(UserscKey uk);

    MyResult deleteuserorder(UserscKey uk);

    MyResult updateUser(User user);

    MyResult updateUserPassword(User user);

    List<User> finduserByName(String username);

    Userbackform finduserbyname(String username);//根据用户名查找后台用户，下面是根据ID

    Userbackform finduserbyid(String id);

    MyResult findAllUser();

    MyResult selectUserProduct(User user);

    MyResult findUserNameorPhone(String username, Long phone);

    MyResult buypro(UserscKey userscKey);

    MyResult buyvip(User user);

    MyResult backuserlogin(Userbackform userbackform);

    MyResult backuserregist(Userbackform userbackform);

    MyResult backuserdelete(Userbackform userbackform);

    MyResult backupdateuser(Userbackform userbackform);

    MyResult addusermessage(Backmessage backmessage);

    MyResult updateusermessage(Backmessage backmessage);

    MyResult updatebackmessage(Backmessage backmessage);

    MyResult addsoftlink(SoftLink softLink);

    MyResult updatesoftlink(SoftLink softLink);

    MyResult deletesoftlink(SoftLink softLink);
}
